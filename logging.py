def log_function_action_entry(func):
    """ Function activity logging """
    def wraps(*args, **kwargs):
        print(f"\t\t\t\tEnter:{func.__name__}")
        result = func(*args, **kwargs)
        print(f"\t\t\t\tExit:{func.__name__}")
        return result
    return wraps
    