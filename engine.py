from actions.console_messages import ConsoleMessages
from actions.error import Error
from actions.quiz import Quiz
from actions.utility import Utility
from file_io import read_task_file

class Engine():
    """Engine to run through task steps """
    def __init__(self, task_file_location, job_description = ""):
        self.job_description = job_description
        self.task_data = read_task_file(task_file_location)
        self.action_mapping = self.set_action_mapping()
        self.job_steps = EngineSteps(self.task_data, self.action_mapping)

    def __str__(self):
        return f'Job Description: {self.job_description}'

    def set_action_mapping(self):
        """ Records the available actions and links their associated steps """
        action_mapping = {
            "console_messages": ConsoleMessages().set_step_mapping(),
            "error": Error().set_step_mapping(),
            "utility": Utility().set_step_mapping(),
            "quiz": Quiz().set_step_mapping(),
            }
        return action_mapping

    def start_engine(self):
        """ Starts the engine to iterate for the steps in chossen job """
        
        job_steps_iter = iter(self.job_steps)
        
        for function, data in job_steps_iter:
            if function:
                function(data)
            else:
                print("No function")
  

class EngineSteps():
    """ Iterator """
    def __init__(self, data, action_mapping):
        self.data = data
        self.counter = 0
        self.action_mapping = action_mapping
         
    def __iter__(self):
        return self

    def __next__(self):
        run_function = None
        if self.counter < len(self.data):
            r_action, r_step, r_data  = self.data[self.counter].values()
            p_action = self.map_action(self.action_mapping, r_action)
            if p_action:
                run_function = self.map_step(p_action, r_step)
            self.counter +=1
            return run_function, r_data
        else:
            raise StopIteration


    def map_action(self, actions_available, current_action):
        """ Maps the current action with the available actions """
        action = actions_available[current_action] if current_action in actions_available else None
        return action

    def map_step(self, steps_available, current_step):
        """ Maps the current step with the available steps """
        step = steps_available[current_step] if current_step in steps_available else steps_available["error"]
        return step
    