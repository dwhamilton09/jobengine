from actions.action import actionImp
from logging import log_function_action_entry

class Error(actionImp):
    """ Class to handle error actions. Contains Error Steps """

    def __str__(self):
        return 'Error'

    @log_function_action_entry
    def print_error(self, data):
        if data:
            print(f"Error: {data['message']}")
        else:
            print(f"Error, no message")
