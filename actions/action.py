from logging import log_function_action_entry
class actionImp():
    """ Action interface for inheritance. Contains Action Steps """

    def __str__(self):
        return 'Undefined'

    def set_step_mapping(self):
        """ Records the available functions """
        function_mapping = {
            "error":self.print_error
            }
        return function_mapping

    def in_development(self):
        raise NotImplementedError()

    @log_function_action_entry
    def print_error(self, data):
        raise NotImplementedError()
