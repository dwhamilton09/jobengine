from actions.action import actionImp
from logging import log_function_action_entry
from time import sleep

class Utility(actionImp):
    """ Class to handle utility actions. Contains Utility Steps """

    def __str__(self):
        return 'Utility'

    def set_step_mapping(self):
        """ Records the available functions """
        step_mapping = {
            "sleep":self.sleep
            }
        return step_mapping

    @log_function_action_entry
    def print_error(self, data):
        if data:
            print(f"Error: {data['message']}")
        else:
            print(f"Error, no message")

    @log_function_action_entry
    def sleep(self, data):
        print(f"Sleep for {data['duration']}")
        duration = int(data['duration'])
        sleep(duration)
   