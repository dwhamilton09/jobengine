from actions.action import actionImp
from logging import log_function_action_entry

class ConsoleMessages(actionImp):
    """ Class to handle writing to console actions. Contains ConsoleMessages Steps """

    def __str__(self):
        return 'ConsoleMessages'

    def set_step_mapping(self):
        """ Records the available functions """
        step_mapping = {
            "print_to_console":self.print_message,
            "error":self.print_error
            }
        return step_mapping

    @log_function_action_entry
    def print_message(self, data):
        print(f"{data['message']}")

    @log_function_action_entry
    def print_error(self, data):
        if data:
            print(f"Error: {data['message']}")
        else:
            print(f"Error, no message")
