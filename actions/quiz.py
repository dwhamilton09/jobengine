from actions.action import actionImp
from logging import log_function_action_entry
from file_io import write_to_file

class Quiz(actionImp):
    """ Class to handle Quizes actions. Contains Quiz Steps """
    def __init__(self):
        super().__init__()
        self.questions = []

    def __str__(self):
        return 'Quiz'

    def set_step_mapping(self):
        """ Records the available functions """
        step_mapping = {
            "question":self.question,
            "error":self.print_error,
            "results":self.results,
            "save":self.save
            }
        return step_mapping

    @log_function_action_entry
    def print_error(self, data):
        if data:
            print(f"Error: {data['message']}")
        else:
            print(f"Error, no message")

    @log_function_action_entry
    def question(self, data):
        user_answer = input(f"Question {data['question']}: ")
        data['correct'] = True if user_answer == data['answer'] else False
        self.questions.append(data)

    @log_function_action_entry
    def results(self, _):
        results_points = [int(question['points']) for question in self.questions if question['correct']]
        sum_results = sum(results_points)
        print(f"{str(sum_results)}")

    @log_function_action_entry
    def save(self, _):
        write_to_file("quiz", self.questions)
