from engine import Engine
from file_io import Config

if __name__ == '__main__':
    engine_config = Config()
    file_location = engine_config.get_job_location()

    job_engine = Engine(file_location, engine_config.get_job_description())
    print(job_engine)

    job_engine.start_engine()
