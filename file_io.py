import yaml
import json
import os
global file_name

class Config():
    """ Configuration settings """
    global file_name
    def __init__(self):
        self.config_details = self.get_config_details()
        self.set_file_name()

    def set_file_name(self):
        global file_name
        file_name = self.config_details['run_job'].split('/')[1]
        file_name = file_name.split('.')[0]

    def get_config_details(self):
        try:
            with open('config.yaml', 'rt') as f:
                text = f.read()
            data = yaml.load(text)
        except:
            data = {}
        return data

    def get_job_location(self):
        return self.config_details.get('run_job','')

    def get_job_description(self):
        return self.config_details.get('job_description','')

    @staticmethod
    def get_file_name():
        return file_name

def read_task_file(task_file_location):
    """ To load the data in the chossen task file """
    try:
        with open(task_file_location) as f:
            data = json.loads(f.read())
    except:
        data = [
            {
                "action":"error",
                "step":"error",
                "data":{"message": "Failed to find file"}
            }
        ]

    return data

def write_to_file(activity, data):
    file_name = f"{Config.get_file_name()}_{activity}.txt"
    path = os.getcwd() + "/out/"+file_name
    with open(path, 'wt') as f:
        data = json.dumps(data)
        f.write(data)
    